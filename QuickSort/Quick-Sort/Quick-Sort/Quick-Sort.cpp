// Quick-Sort.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
int partition(int *a, int p, int size) { //va aranja elementele mai mici in stanga(decat ultimul element) si  mai mari in dreapta
	int x = a[size]; //pivot(alegem ultimul element ca pivot)
	int i = p - 1;
	for (int j=p;j<=size-1;j++)
		if (a[j] <= x) {
			i = i + 1;
			int aux = a[i];
			a[i] = a[j];
			a[j] = aux;
		}
	int aux1 = a[i + 1];
	a[i + 1] = a[size];
	a[size] = aux1;
	return i + 1;
}

void QuickSort(int *a, int p, int size) {
	if (p < size) {
		int q = partition(a, p, size);
		QuickSort(a, p, q - 1);
		QuickSort(a, q+1, size);
	}

}

int main()
{
	int A[1000] = { 3,1,2,7,6,5 };
	int size = 6;
	QuickSort(A, 0, size-1);
	for (int i = 0; i < 6; i++)
		printf("%d ", A[i]);
	return 0;
}

