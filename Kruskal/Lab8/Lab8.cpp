#include <stdio.h>
#include "stdafx.h"
#include <stdlib.h>


typedef struct node {
	int data;
	int rank;
	struct node* parent;
}Node;

typedef struct edge {
	Node *node1;
	Node *node2;
	int cost;
}Edge;

typedef struct graph {
	int V; //nr noduri
	int E; //nr muchii
}Graph;

Node* make_set(int key) {// se formeaza fiecare nod
	Node* node = (Node*)malloc(sizeof(Node));
	node->data = key;
	node->rank = 0;
	node->parent = node;  //el insusi este parintele lui 
	return node;
}

Node* find_set(Node* x) {
	if ((x) != (x->parent)) {
		return find_set(x->parent);
	}
	return x->parent;
}

void link(Node* x, Node* y) {
	if (x->rank > y->rank) {
		y->parent = x;
	}
	else {
		x->parent = y;
		if (x->rank == y->rank) {
			y->rank = y->rank + 1;
		}
	}
}

void uni(Node* a, Node* b) {
	link(find_set(a), find_set(b));
}

int partitionare(Edge a[], int start, int stop) {
	int pivot = a[stop].cost;
	int i = start;
	int j;
	Edge aux;
	for (j = start; j <= stop - 1; j++) {
		if (a[j].cost <= pivot) {
			aux = a[i];
			a[i] = a[j];
			a[j] = aux;
			i++;
		}

	}
	aux = a[stop];
	a[stop] = a[i];
	a[i] = aux;
	return i;
}

void sortQ(Edge a[], int start, int finish) {
	int piv;
	if (start < finish) {
		piv = partitionare(a, start, finish);
		sortQ(a, start, piv - 1);
		sortQ(a, piv + 1, finish);

	}

}
Edge muchii[100];

void Kruskal(Graph* G) {
	Edge result[15];
	//for (int i = 0; i < 15; i++)
		//result[i] = (Edge*)malloc(sizeof(Edge));
	int k = 0;
	sortQ(muchii, 0, G->E - 1);
	for (int i = 0; i < G->E - 1; i++) {
		if (find_set(muchii[i].node1) != find_set(muchii[i].node2)) {
			result[k] = muchii[i];
			uni(muchii[i].node1, muchii[i].node2);
			k++;
		}
	}

	for (int i = 0; i < k; i++) {
		printf("Muchie intre %d - %d cu costul: %d\n", result[i].node1->data, result[i].node2->data, result[i].cost);
	}
}

int main()
{
	Node* v[10];
	int nr = 10;

	printf("MAKE SET:\n");
	for (int i = 0; i < nr; i++) {
		v[i] = make_set(i);
	}
	for (int i = 0; i < nr; i++) {
		printf("Parintele %d si cheia %d\n", v[i]->parent->data, v[i]->data);
	}

	printf("\nUNION:\n");
	uni(v[1], v[0]);
	uni(v[2], v[3]);
	uni(v[0], v[3]);
	uni(v[4], v[3]);
	for (int i = 0; i < nr; i++) {
		printf("Parinte: %d  nod: %d\n", v[i]->parent->data, v[i]->data);
	}

	printf("\nFIND SET:\n");
	for (int i = 0; i < nr; i++) {
		printf("Parinte: %d nod: %d\n", find_set(v[i])->data, v[i]->data);
	}

	Graph* Graf = (Graph*)malloc(sizeof(Graph));
	Graf->E = 10;
	Graf->V = 7;
	Node** noduri = (Node**)malloc(sizeof(Node*));
	//Edge muchii[100];//modificat dimensiunea "sa incapa"

	for (int i = 0; i < Graf->V; i++) {
		noduri[i] = make_set(i);
	}
	//for (int i = 0; i < Graf->E; i++) {
	//	muchii[i] = (Edge*)malloc(sizeof(Edge));//aici
	//}

	muchii[0].node1 = noduri[0];
	muchii[0].node2 = noduri[1];
	muchii[0].cost = 1;

	muchii[1].node1 = noduri[2];
	muchii[1].node2 = noduri[3];
	muchii[1].cost = 2;

	muchii[2].node1 = noduri[2];
	muchii[2].node2 = noduri[1];
	muchii[2].cost = 3;

	muchii[3].node1 = noduri[0];
	muchii[3].node2 = noduri[2];
	muchii[3].cost = 4;

	muchii[4].node1 = noduri[3];
	muchii[4].node2 = noduri[5];
	muchii[4].cost = 5;

	muchii[5].node1 = noduri[3];
	muchii[5].node2 = noduri[4];
	muchii[5].cost = 6;

	muchii[6].node1 = noduri[3];
	muchii[6].node2 = noduri[1];
	muchii[6].cost = 7;

	muchii[7].node1 = noduri[1];
	muchii[7].node2 = noduri[4];
	muchii[7].cost = 8;

	muchii[8].node1 = noduri[5];
	muchii[8].node2 = noduri[6];
	muchii[8].cost = 9;

	muchii[9].node1 = noduri[4];
	muchii[9].node2 = noduri[5];
	muchii[9].cost = 10;

	printf("\nBEFORE KRUSKAL:\n");
	for (int i = 0; i < Graf->E; i++) {
		printf("Muchie intre %d - %d cu costul: %d\n", muchii[i].node1->data, muchii[i].node2->data, muchii[i].cost);
	}

	printf("\nAFTER KRUSKAL:\n");
	Kruskal(Graf);
	free(Graf);
	//getchar();
	printf("ajunge?!!?!\n");
}

