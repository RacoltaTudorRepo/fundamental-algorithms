// HEAP-BOTTOMUP.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
int comp, atrib;
int parent(int i) {
	return i / 2;
}

int right(int i) {
	return 2 * i+1;
}

int left(int i) {
	return 2 * i;
}

void interschimbareFiu(int *A, int i, int smallest) {
	int aux = A[i];
	A[i] = A[smallest];
	A[smallest] = aux;
}
void maxHeapify(int *A, int i, int heapSize) {
	int largest;
	int l = left(i);
	int r = right(i);
	if (l <= heapSize && A[l] >= A[i])
	{
		largest = l;
		comp++;
	}
		else largest = i;
	if (r <= heapSize && A[r] >= A[largest])
	{
		largest = r;
		comp++;
	}
	if (largest != i)
	{
		atrib += 3;
		interschimbareFiu(A, i, largest);
		maxHeapify(A, largest, heapSize);
	}
}

void constructieBottomUp(int *A,int heapSize) {
	for (int i = heapSize / 2; i >= 0; i--)
		maxHeapify(A, i,heapSize);
}

void heapSort(int *A, int heapSize) {
	constructieBottomUp(A, heapSize);
	for (int i = heapSize; i >= 1; i--)
	{
		interschimbareFiu(A, 0, i);
		heapSize--;
		maxHeapify(A, 0, heapSize);
	}
}

int main()
{
	int A[1000];
	for (int i = 0; i < 5; i++)
		A[i] = i;
	int heapSize = 10;
	int sir[] = { 16, 4, 10, 14, 7, 9, 3, 2, 8, 1 };
	constructieBottomUp(sir,heapSize-1);
	for (int i = 0; i < heapSize; i++)
		printf_s("%d ", sir[i]);
	printf_s("\n");
	heapSort(sir, heapSize-1);
	for (int i = 0; i < heapSize; i++)
		printf_s("%d ", sir[i]);
	return 0;
}

