// HEAP-TOPDOWN.cpp : Defines the entry point for the console application.
//
#include "stdafx.h"
int comp, atrib;
int parent(int i) {
	return i / 2;
}
void interschimbareFiu(int *heap, int indexFiu, int indexParinte ) {
	int aux;
	aux = heap[indexFiu];
	heap[indexFiu] = heap[indexParinte];
	heap[indexParinte] = aux;
}


void heapIncreaseKey(int *A, int sizeHeap, int element)
{
	A[sizeHeap] = element;
	atrib++;
	comp++;
	while ((sizeHeap > 0) && (A[parent(sizeHeap)] < A[sizeHeap]))
	{
		comp++;
		atrib += 3;
		interschimbareFiu(A, sizeHeap, parent(sizeHeap));
		sizeHeap = parent(sizeHeap);
	}
}

void maxHeapInsert(int *A, int element, int size) {
	heapIncreaseKey(A, size, element);
}

void heapBuildTopDown(int *A,int heapSize)
{
	int size = -1;
	for (int i = 0; i < heapSize; i++)
	{
		size++;
		maxHeapInsert(A, A[i], size);
	}
}


int main()
{
	comp = 0;
	atrib = 0;
	int A[100]={1,2,3,4,5 }; 
	int heapSize = 5;
	heapBuildTopDown(A, 5);
	for (int i = 0; i < heapSize; i++)
		printf_s("%d ", A[i]);
	printf("\n %d %d", comp, atrib);

    return 0;
}

