// TOATE.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
int parent(int i) {
	return i / 2;
}

int right(int i) {
	return 2 * i + 1;
}

int left(int i) {
	return 2 * i;
}

int compTD, compBU, atribTD, atribBU;

void interschimbareFiu(int *heap, int indexFiu, int indexParinte) {
	int aux;
	aux = heap[indexFiu];
	heap[indexFiu] = heap[indexParinte];
	heap[indexParinte] = aux;
}


void heapIncreaseKey(int *A, int sizeHeap, int element)
{
	A[sizeHeap] = element;
	atribTD++;
	compTD++;
	while ((sizeHeap > 0) && (A[parent(sizeHeap)] < A[sizeHeap]))
	{
		compTD++;
		atribTD += 3;
		interschimbareFiu(A, sizeHeap, parent(sizeHeap));
		sizeHeap = parent(sizeHeap);
	}
}

void maxHeapInsert(int *A, int element, int size)
{
	heapIncreaseKey(A, size, element);
}

void constructieTopDown(int *A, int heapSize)
{
	int size = -1;
	for (int i = 0; i < heapSize; i++)
	{
		size++;
		maxHeapInsert(A, A[i], size);
	}
}





void maxHeapify(int *A, int i, int heapSize) {
	int largest;
	int l = left(i);
	int r = right(i);
	compBU++;
	if (l <= heapSize && A[l] >= A[i])
		largest = l;
	else largest = i;
	compBU++;
	if (r <= heapSize && A[r] >= A[largest])
		largest = r;
	if (largest != i)
	{	
		atribBU += 3;
		interschimbareFiu(A, i, largest);
		maxHeapify(A, largest, heapSize);
	}
}

void constructieBottomUp(int *A, int heapSize) {
	for (int i = heapSize / 2; i >= 0; i--)
		maxHeapify(A, i, heapSize);
}


void heapSort(int *A, int heapSize) {
	constructieBottomUp(A, heapSize);
	for (int i = heapSize; i >= 1; i--)
	{
		interschimbareFiu(A, 0, i);
		heapSize--;
		maxHeapify(A, 0, heapSize);
	}
}





int main()
{
	FILE *f1, *f2;
	//fopen_s(&f1, "MEDIU-BU.csv", "w");
	//fopen_s(&f2, "MEDIU-TD.csv", "w");
	int a[10000],b[10000];
	int i,n,j;
	printf_s("Constructie heap Bottom UP\n");
	int A[100] = { 3,4,2,1,5 };
	int heapSize = 5;
	constructieBottomUp(A, heapSize );
	for (int i = 0; i < 5; i++)
		printf_s("%d ", A[i]);
	printf_s("\n");
	heapSort(A, heapSize - 1);
	for (int i = 0; i < 5; i++)
		printf_s("%d ", A[i]);


	printf_s("\nConstructie heap Top Down\n");
	int B[100] = { 3,4,2,1,5 };
	heapSize = 5;
	constructieTopDown(B, heapSize );
	for (int i = 0; i < 5; i++)
		printf_s("%d ", B[i]);
	printf_s("\n");
	heapSort(B, heapSize - 1);
	for (int i = 0; i < 5; i++)
		printf_s("%d ", B[i]);

	/*for (j = 1; j <= 5; j++) 
	{
		fprintf_s(f1, "\nMasurarea%d,Masurarea%d,Masurarea%d\n", j, j,j);
		fprintf_s(f2, "\nMasurarea%d,Masurarea%d,Masurarea%d\n", j, j,j);
		for (n = 100; n <= 10000; n += 500)
		{
			compTD = 0; compBU = 0; atribTD = 0; atribBU = 0;
			for (i = 0; i < n; i++)
			{
				a[i] = rand() % n;
				b[i] = a[i];
			}
			constructieBottomUp(a, n - 1);
			fprintf_s(f1, "%d,%d,%d\n", compBU, atribBU,compBU+atribBU);
			constructieTopDown(b, n - 1);
			fprintf_s(f2, "%d,%d,%d\n", compTD, atribTD,compTD+atribTD);
		}
	}*/
	/*fopen_s(&f1, "DEFAV-BU.csv", "w");
	fopen_s(&f2, "DEFAV-TD.csv", "w");
	for (n = 100; n <= 10000; n += 500)
	{
		compTD = 0; compBU = 0; atribTD = 0; atribBU = 0;
		for (i = 0; i < n; i++)
		{
			a[i] = i;
			b[i] = a[i];
		}
		constructieBottomUp(a, n - 1);
		fprintf_s(f1, "%d,%d,%d\n", compBU, atribBU, compBU + atribBU);
		constructieTopDown(b, n - 1);
		fprintf_s(f2, "%d,%d,%d\n", compTD, atribTD, compTD + atribTD);
	}*/
    return 0;
}

