/* Racolta Tudor Ioan-grupa 30226: Tema5 - Tabele de dispersie
	GENERARE REZULTATE:
		In cadrul acestei teme, am avut de folosit tabele de dispersie pentru a imprastia datele intr-un vector, astfel incat sa gasim un loc potrivit pentru fiecare element, atata timp
cat tabela este goala. Rezultatele au fost generate in felul urmator: am efectuat 5 masuratori pentru fiecare dintre cei 5 factori de umplere ceruti, iar generarea simularii a fost efectuata
de functia generateSim care funtioneaza in felul urmator:vom aloca tabela de dispersie de 10007 elemente si vom aloca 2 array-uri, array1 va fi 10007+1500 iar array2 de 1500. In continuare,
pentru a vedea elemente gasite/negasite, am introdus din array1 -unde sunt elemente random de la 0 la 30000 - size elemente (size fiind factor umplere, ex 8000, 8500..), si vom ramane
cu cel putin 1500 de elemente neintroduse. Mai apoi, vom folosi array2, care este un array de pozitii (ne va da pozitii de la 0 la size-1, de exemplu a[2]=8000), pentru a determina
care elemente sunt introduse in tabela (introducand de la 0 la size, stim sigur ca orice element array1[array2[k]], PTR K DE LA 0 LA SIZE !, se afla in tabela). Mai ramane sa efectuam 
cautare pentru cele 1500 care nu se gasesc, cautare efectuata prin cautarea elementelor de la size la size+1500 din array1).
	INTERPRETARE REZULTATE:
		In general, odata cu cresterea factorului de umplere, operatiile de cautare vor da mai multe coliziuni, ceea ce inseamna ca vom avea mai multe accese la nivel de celule(practic, vom
verifica in mai multe celule daca acel element exista). Din rezultatele pe care le-am obtinut, un lucru sigur este faptul ca media efortului depus pentru elemente gasite/negasite, in urma a 5 
masurari, este in crestere odata cu cresterea factorului de umplere, iar tot din rezultatele obtinute, efortul este de cel putin 4 ori mai mare in cazul elementelor negasite. Totusi, daca ne gamdim
ca poate acest lucru ar trebui sa fie normal si in cazul maximului de coliziuni pentru gasite/negasite pe fiecare factor de umplere, acest lucru nu este tot timpul asa, deoarecere cautarea
depinde in exclusivitate de cum sunt generate datele si ce elemente urmeaza sa cautam in tabela de dispersie, fapt ce nu ne garanteaza neaparat ca odata cu cresterea factorului de umplere,
o sa creasca si maximul de accese pentru elemente gasite/negasite. Un lucru care este mai sigur din acest punct de vedere este faptul ca maximul de accese pentru elemente negasite va fi
cel putin maximul de accese pentru elemente gasite.
	
	
*/

#include "stdafx.h"
#include "Profiler.h"

int total, totalN, max, maxN;
typedef struct cell {
	int val;
	int status;
}Cell;
enum{FREE,OCCUPIED}; //m- dimensiunea tabelei; k-valoarea ce trebuie introdusa in tabela 
int hash_table_occupied(Cell *T, int m) {
	int i;
	for (i = 0; i < m; i++) {
		if (T[i].status == FREE) return 0;
	}
	return 1;
}
int quadratic_probing(int k, int m, int i) {
	return (k%m + i + i*i) % m;
}
void insert_quadratic_probing(int k, Cell *T, int m) {
	if (hash_table_occupied(T, m)) {
		printf("Nu se mai poate insera cheia %d, tabela este plina \n", k);
		return;
	}
	int i = 0;
	int collision = 1;
	int h = 0;
	do {
		h = quadratic_probing(k, m, i);
		if (T[h].status == FREE)
			collision = 0;
		else i++;
	} while (collision);
	T[h].status = OCCUPIED;
	T[h].val = k;
}
void afisare(Cell *T, int m) {
	int i;
	printf("\n\nTabela de dispersie este \n");
	for (i = 0; i < m; i++) {
		if (T[i].status == OCCUPIED)
			printf("T[%d]=%d\n", i, T[i].val);
		else
			printf("T[%d]= --\n", i);

	}
}
void set_table_free(Cell *T, int m) {
	//initializam tabela
	int i;
	for (i = 0; i<m; i++) {
		T[i].status = FREE;
	}

}
int cautare(int k, Cell *T, int m,int &collision) {
	int i = 0;
	int h;
	do{
		h = quadratic_probing(k, m, i);
		collision = i;
		if (T[h].val == k)
			return h;
		i++;
	} while (T[h].status != FREE && i < m);
	return -1;
}
void generateSim(int size,int m) {
	Cell *T = (Cell*)malloc(sizeof(Cell)*m);
	int *array1 = (int*)malloc((m + 1500) * sizeof(int));
	int *array2 = (int*)malloc(1500 * sizeof(int));

	for (int i = 0; i < 5; i++) {	//facem 5 masuratori
		set_table_free(T, m);
		FillRandomArray(array1, size + 1500, 0, 30000, true, 0); //array de elemente
		FillRandomArray(array2, 1500, 0, size - 1, true, 0); //array de pozitii
		for (int k = 0; k < size; k++) { //vom introduce de la 0 la size (8000..etc, cat avem factorul de implere) --> raman 1500 neintroduce
			insert_quadratic_probing(array1[k],T,m);
		}
		
		for (int k = 0; k < 1500; k++) { //cautam 1500 de elemente introduse (array2[k] ne da pozitii care stim sigur ca sunt ocupate)
			int collisions = 0;
			cautare(array1[array2[k]], T, m, collisions);
			total += collisions;
			if (max < collisions) max = collisions;
		}
		
		for (int i = size; i < size + 1500; i++) { //cautam 1500 de elemente care nu au fost introduse
			int collisionsN = 0;
			cautare(array1[i], T, m, collisionsN);
			totalN += collisionsN;
			if (maxN < collisionsN) maxN = collisionsN;
		}
	}
	free(T);
}
int main()
{
	//FILE *f;
	//fopen_s(&f, "evaluare.csv", "w");
	//fprintf(f,"Factor umplere,Maxim gasit,Maxim negasit,Media gasite,Media negasite\n");
	/*int a[100];
	int m = 100;
	Cell *T = (Cell*)malloc(m * sizeof(Cell)); 
	set_table_free(T, m);
	afisare(T, m);
	insert_quadratic_probing(3, T, m);
	insert_quadratic_probing(12, T, m);
	insert_quadratic_probing(5, T, m);
	insert_quadratic_probing(8, T, m);

	afisare(T, m);
	printf("Elementul %d in T[%d]", 5, cautare(5, T, m));*/


	int col;
	int a[95];
	int m = 100;
	Cell *T = (Cell*)malloc(m * sizeof(Cell));
	set_table_free(T, m);
	for (int i = 0; i < m*0.95; i++)
	{
		int elem = rand() % 1000;
		insert_quadratic_probing(elem, T, m);
		a[i] = elem;
	}
	afisare(T, m);
	printf("Elementul %d s-a gasit pe pozitia %d\n",a[2],cautare(a[2], T, m,col));
	printf("Elementul %d s-a gasit pe pozitia %d\n", 2000, cautare(2000, T, m, col));
	

	/*total=totalN = max = maxN = 0;
	generateSim(8000, 10007);
	fprintf(f,"8000,%d,%d,%f,%f\n", max, maxN, double(total / 5) / 1500, double(totalN / 5) / 1500);

	total = totalN = max = maxN = 0;
	generateSim(8500, 10007);
	fprintf(f,"8500,%d,%d,%f,%f\n", max, maxN, double(total / 5) / 1500, double(totalN / 5) / 1500);

	total = totalN = max = maxN = 0;
	generateSim(8500, 10007);
	fprintf(f,"9000,%d,%d,%f,%f\n", max, maxN, double(total / 5) / 1500, double(totalN / 5) / 1500);

	total = totalN = max = maxN = 0;
	generateSim(8500, 10007);
	fprintf(f,"9500,%d,%d,%f,%f\n", max, maxN, double(total / 5) / 1500, double(totalN / 5) / 1500);
	
	total = totalN = max = maxN = 0;
	generateSim(8500, 10007);
	fprintf(f,"9900,%d,%d,%f,%f\n", max, maxN, double(total / 5) / 1500, double(totalN / 5) / 1500);*/



    return 0;
}

