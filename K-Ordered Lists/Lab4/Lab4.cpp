/*Tudor Racolta Ioan, grupa 30226 - Tema 4: Interclasarea a N elemente distribuite in K liste
INTERPRETAREA REZULTATELOR:
1)N-variabil, K-variabil(5,10,100) 
Acest caz s-a generat alegand initial un K si pentru fiecare dimensiune N, s-a generat un sir ordonat crescator, urmand apoi a se distribui aleator in cele K liste.
Se poate observa ca odata cu cresterea numarului de liste, algoritmul va fi din ce in ce mai ineficien, ceea ce este si normal datorita efortului depus de operatiile asupra heap-ului
(operatii de push si pop care implica reconstruirea heap-ului la fiecare pas). Astfel, eficienta algoritmul este O(nlogK), diferenta dintre cele 3 drepte prezente pe grafic fiind data 
de logK.

2)N-10000, K-variabil 
Acest caz a fost generat prin generare unui sir de 10000 ordonat crescator iar mai apoi, pentru fiecare K, au fost distribuite in mod aleator elementele pe liste(indexul ei fiind determinat
de functia rand() )
La fel ca si in cazul anterior, pastrand datele generate initial si schimband doar numarul listelor, se poate observa mai clar eficienta O(nlogK) a algoritmului pe masura ce crestem
numarul de liste datorita curbei logaritmice prezenta in grafic. 
*/


#include "stdafx.h"
int atrib, comp;
//Metode pentru liste
typedef struct node {
	int key;
	struct node *next;
} NodeT;
typedef struct ElementHeap {
	int data;
	int lista;
}ElementHeap;
NodeT* newNode(int data) {
	NodeT* nodNou = (NodeT*)malloc(sizeof(NodeT));
	nodNou->key = data;
	nodNou->next = NULL;
	return nodNou;
}
void print_list(NodeT *head) {
	NodeT *q = head;
	while (q != NULL)
	{
		printf("%d  ", q->key);
		q = q->next;
	}
	printf("\n");
}
void insert_last(NodeT **head, NodeT **last, int givenKey) {
	NodeT *nodNou= newNode(givenKey);
	if (*last == NULL) {
		*head = nodNou;
		*last = nodNou;
	}
	else {
		(*last)->next = nodNou;
		*last = nodNou;
	}
}
int delete_first(NodeT **head, NodeT **last) {
	int key;
	if (*head != NULL) {
		NodeT *p = *head;
		*head = (*head)->next;
		key = p->key;
		free(p);
		if (*head == NULL)
			*last = NULL;
		return key;
	}
	return NULL;
}

//Metode pentru Heap
int sizeHeap = 0;
int right(int i) {
	return 2 * i + 1;
}
int left(int i) {
	return 2 * i;
}
int parent(int i) {
	return i / 2;
}
void interschimbareFiu(ElementHeap *heap, int indexFiu, int indexParinte) {
	ElementHeap aux;
	aux = heap[indexFiu];
	heap[indexFiu] = heap[indexParinte];
	heap[indexParinte] = aux;
}
void minHeapify(ElementHeap *A, int i, int heapSize) {
	int smallest;
	int l = left(i);
	int r = right(i); comp++;
	if (l <= heapSize && A[l].data < A[i].data)
	{
		smallest = l;
		
	}
	else smallest = i; comp++;
	if (r <= heapSize && A[r].data < A[smallest].data)
	{
		smallest = r;
		
	}
	if (smallest != i)
	{
		atrib += 3;
		interschimbareFiu(A, i, smallest);
		minHeapify(A, smallest, heapSize);
	}
}
void heapIncreaseKey(ElementHeap *A, int sizeHeap, int element)
{
	A[sizeHeap].data = element;
	atrib++;
	comp++;
	while ((sizeHeap > 0) && (A[parent(sizeHeap)].data > A[sizeHeap].data))
	{
		comp++;
		atrib += 3;
		interschimbareFiu(A, sizeHeap, parent(sizeHeap));
		sizeHeap = parent(sizeHeap);
	}
}
void minHeapInsert(ElementHeap *A, int element, int &sizeHeap) {
	heapIncreaseKey(A, sizeHeap, element);
	sizeHeap++;
};
ElementHeap ExtractMin(ElementHeap *a, int &size) {
	ElementHeap min = a[0];
	if (size != 1) {
		a[0] = a[(size)-1];
		atrib++;
	}
	size--;
	minHeapify(a, 0, size);
	return min;
}

//Merge Lists
void merge(NodeT **first, NodeT **last, int k) {
	atrib = 0;
	comp = 0;
	ElementHeap minHeap[10000];	
	NodeT *head = NULL;
	NodeT *tail = NULL;;
	for (int i = 0; i < k; i++) {
		int elementLista = delete_first(&first[i], &last[i]);
		minHeap[sizeHeap].lista = i;
		minHeapInsert(minHeap, elementLista, sizeHeap);
	}
	//printf("%d", sizeHeap);

	while (sizeHeap > 0) {
		ElementHeap minim = ExtractMin(minHeap, sizeHeap);
		insert_last(&head, &tail, minim.data); atrib++;
		comp++;
		if (first[minim.lista]!=NULL) {
			int elementLista = delete_first(&first[minim.lista], &last[minim.lista]); atrib++;
			minHeap[sizeHeap].lista = minim.lista;
			minHeapInsert(minHeap, elementLista, sizeHeap);
		}
	}
	//printf("final list: \n");
	//printList(head);
}

int main()
{
	FILE *f;
	/*int k = 4;
	NodeT *first[4] = { NULL };
	NodeT *last[4] = { NULL };
	int a[20];
	for (int i = 0; i < 20; i++)
		a[i] = i;

	for (int i = 0; i < 20; i++) {
		int list = rand() % k;
		insert_last(&first[list], &last[list], a[i]);
	}
	printList(first[0]);
	printList(first[1]);
	printList(first[2]);
	printList(first[3]);
	merge(first, last, k);
	printf("Atrib %d Comp %d", atrib, comp);*/

	/*fopen_s(&f, "mergeEx1.csv", "w");
	int k = 5;
	int a[10000];
	NodeT *first[5] = { NULL };
	NodeT *last[5] = { NULL };
	fprintf(f, "N,Atrib,Comp,Total,K=5\n");
	for (int n = 100; n <= 10000; n += 400) {
		for (int i = 0; i < n; i++)
			a[i] = i;
		for (int i = 0; i < n; i++) {
			int list = rand() % k;
			insert_last(&first[list], &last[list], a[i]);
		}
		merge(first, last, k);
		fprintf(f,"%d,%d,%d,%d\n", n, atrib, comp, atrib + comp);
	}*/

	/*fopen_s(&f, "mergeEx1.1.csv", "w");
	int k = 10;
	int a[10000];
	NodeT *first[10] = { NULL };
	NodeT *last[10] = { NULL };
	fprintf(f, "N,Atrib,Comp,Total,K=10\n");
	for (int n = 100; n <= 10000; n += 400) {
		for (int i = 0; i < n; i++)
			a[i] = i;
		for (int i = 0; i < n; i++) {
			int list = rand() % k;
			insert_last(&first[list], &last[list], a[i]);
		}
		merge(first, last, k);
		fprintf(f, "%d,%d,%d,%d\n", n, atrib, comp, atrib + comp);
	}*/
		
	/*fopen_s(&f, "mergeEx1.2.csv", "w");
	int k = 100;
	int a[10000];
	NodeT *first[100] = { NULL };
	NodeT *last[100] = { NULL };
	fprintf(f, "N,Atrib,Comp,Total,K=100\n");
	for (int n = 100; n <= 10000; n += 400) {
		for (int i = 0; i < n; i++)
			a[i] = i;
		for (int i = 0; i < n; i++) {
			int list = rand() % k;
			insert_last(&first[list], &last[list], a[i]);
		}
		merge(first, last, k);
		fprintf(f, "%d,%d,%d,%d\n", n, atrib, comp, atrib + comp);
	}*/


	fopen_s(&f, "mergeEx2.csv", "w");
	int n = 10000;
	int a[10000];
	for (int i = 0; i < n; i++)
		a[i] = i;
	fprintf(f, "K,Atrib,Comp,Total,n=10000\n");
	for (int k = 10; k <= 500; k += 10) {
		NodeT *first[500] = { NULL };
		NodeT *last[500] = { NULL };
		for (int i = 0; i < n; i++) {
			int list = rand() % k;
			insert_last(&first[list], &last[list], a[i]);
		}
		merge(first, last, k);
		fprintf(f, "%d,%d,%d,%d\n", k, atrib, comp, atrib + comp);
	}


	//Teste initiale push si pop pe heap-max
	/*int a[100] = { 5,3,7,10,1 };
	maxHeapInsert(a, a[0], sizeHeap);
	maxHeapInsert(a, a[1], sizeHeap);
	maxHeapInsert(a, a[2], sizeHeap);
	maxHeapInsert(a, a[3], sizeHeap);
	maxHeapInsert(a, a[4], sizeHeap);
	printf("Sizeul este %d \n",sizeHeap);
	for (int i = 0; i <= sizeHeap; i++)
	printf("%d ", a[i]);
	printf("\nMaximul extras: %d\n", ExtractMax(a, sizeHeap));
	for (int i = 0; i <= sizeHeap; i++)
	printf("%d ", a[i]);*/

	return 0;
};

