/**
 *
 * @author Novacean Nicoleta Ligia
 * @group 30424
 *
 * Problem specification: Multi-way Trees
 *
 *   In order to obtain the multi-way tree representation(R2), I employed a data structure containing an int(the key of the node)
 * and an array of nodes which represent the children. These data structures are, altogether, stored into an array.
 * The second component is a vector because children are not directly linked one to the other one. For the binary tree
 * representation, I employed a data structure containing the key and two pointers: one for the firstChild(left link) and one
 * for the nextSibling(right link).
 *  The first transformation(T1) transforms the initial parent representation into a multi-way tree. The procedure consists
 * of 2 important steps: firstly, it initializes all the keys in the array of NodeM nodes to their proper value and, secondly,
 * it places in the array of children the nodes whose parent has the same value as the current key. Therefore, the
 * complexity is O(n).
 *  The second transformation(T2) converts the multi-way tree into a binary tree. In order to accomplish this, a non-recursive
 * approach has been used. The nodes of the tree are stored into an initialized array. Then, by traversing each node of the
 * multi-way tree and its list of children, we update the firstChild and the nextSibling pointers. The complexity is O(n)
 * because each node(except for the root) is traversed at most twice: once when it is a key in the multi-way tree representation
 * and once when it appears in the vector of children.
 *  The additional memory required by the implemented algorithms is an array, named positions, where the number of children of
 * each node is stored. This is of great use for obtaining boundary conditions and for assuring algorithms' functionality.
 *  For each of the 3 implementations, a prettyPrint procedure is available. It reveals a tree structure. All of the printing
 * algorithms generate the same result(i.e. the same way of displaying data), but they are implemented differently, in accordance
 * with the used data representation.
 *
 */

#include <iostream>
#include <cstdlib>
#include <cstdio>

#define DIMENSION 9

using namespace std;

typedef struct Node {
    int key;
    Node* childrenNodes[DIMENSION];
} NodeM;

typedef struct Node1 {
    int key;
    Node1* firstChild;
    Node1* nextSibling;
} NodeB;

int parents[DIMENSION + 1] = {0, 2, 7, 5, 2, 7, 7, -1, 5, 2};
NodeM multiwayTree[DIMENSION + 1];
NodeB* binaryTree[DIMENSION + 1];
int position[DIMENSION + 1];


void T1() {

    position[0] = 0;
    for (int i = 1; i <= DIMENSION; i++) {
        position[i] = 1;
    }

    for (int i = 1; i <= DIMENSION ; i++) {
        multiwayTree[i].key = i;
    }

    for (int i = 1; i <= DIMENSION; i++) {
        multiwayTree[parents[i]].childrenNodes[position[parents[i]]] = &multiwayTree[i];
        position[parents[i]] += 1;
    }
}


NodeB* allocateNode() {
    NodeB* newNode = (NodeB*)malloc(sizeof(NodeB));
    newNode->firstChild = NULL;
    newNode->nextSibling = NULL;
    return newNode;
}


NodeB* T2(int rootKey) {

    for (int i = 1; i <= DIMENSION; i++) {
        NodeB* nodeB = allocateNode();
        nodeB->key = i;
        //nodeB->firstChild = NULL;
        //nodeB->nextSibling = NULL;
        binaryTree[i] = nodeB;
    }

    for (int i = 1; i <= DIMENSION; i++) {
        if (position[i] != 1) {
             binaryTree[i]->firstChild = binaryTree[multiwayTree[i].childrenNodes[1]->key];
        }
        if (position[i] > 2) {
            for (int j = 2; j < position[i]; j++) {
                binaryTree[multiwayTree[i].childrenNodes[j-1]->key]->nextSibling = binaryTree[multiwayTree[i].childrenNodes[j]->key];
            }
        }
    }

    return binaryTree[rootKey];
}


int findRootKey() {
    int root = 0;
    for (int i = 1; i <= DIMENSION; i++)
        if (parents[i] == -1)
            root = i;
    return root;
}


NodeM obtainRoot(int key) {
    NodeM root;
    for (int i = 0; i <= DIMENSION; i++) {
        if (multiwayTree[i].key == key )
            root = multiwayTree[i];
    }
    return root;
}



void prettyPrintR1() {
    for (int i = 1; i <= DIMENSION; i++) {
        printf("%d " , parents[i]);
    }
}


void prettyPrintR1(int key, int level) {
    for (int i = 0; i < level; i++)
        printf("   ");
    printf("%d\n", key);
    for (int i = 1; i <= DIMENSION; i++)
        if (parents[i] == key)
            prettyPrintR1(i, level + 1);
}


void prettyPrintR2(int key, int level) {
    for (int i = 0; i < level; i++)
        printf("   ");
    printf("%d\n", key);
    for (int j = 1; j < position[key]; j++)
        prettyPrintR2(multiwayTree[key].childrenNodes[j]->key, level + 1);
}


void prettyPrintR3(NodeB* root, int level) {
    if (root != NULL) {
        for (int i = 0; i < level; i++)
            printf("   ");
        printf("%d\n", root->key);
        prettyPrintR3(root->firstChild, level+1);
        prettyPrintR3(root->nextSibling, level);
    }
}

int main()
{
    int rootKey = findRootKey();
    printf("The parents vector is: \n");
    prettyPrintR1();
    printf("\nR1: \n");
    prettyPrintR1(rootKey, 0);

    T1();
    printf("\nR2: \n");
    prettyPrintR2(rootKey, 0);

    NodeB* rootB = (NodeB*)malloc(sizeof(NodeB));
    rootB = T2(rootKey);
    printf("\nR3: \n");
    prettyPrintR3(rootB, 0);
    return 0;
}
