/*Racolta Tudor Ioan, grupa 30226: Tema 7- Arbori multicai
DESCRIERE ABORDARE:
Reprezentarea 1) 
	Aceasta reprezentarea consta intr-un arbore bazat pe relatie parinte-copil, fiind reprezentata sub forma unui vector de tati. Practic pentru un array parrent[i], i este copilul, iar continutul 
parent[i] este parintele acestuia, unde -1 reprezinta radacina arborelui. In cadrul acestei reprezentari nu am mai facut transformari suplimentare, fiind usor de afisat pe nivele, apeland treptat
pretty print-ul pentru fiecare fiu din vectorul de tati.

Reprezentarea 2)
	Pentru a obtine aceasta reprezentare sub forma de vectori de copii, am creat o structura noua pentru fiecare nod din arbore care contine cheia nodului si un vector de noduri de copii. Pentru a putea
efectua transformarea, am creat un array indexChildren care va tine intotdeauna pe pozitia i unde urmeaza sa punem un copil, pe ce loc in vector. Practic pentru a efectua transformarea, vom parcurge
vectorul de parinti si pentru cheile care nu sunt radacina (diferit de -1) vom crea legatura intre copil si parinte, practic vom pune nodul multi[i] ca si copil pentru nodul multi[parent[i]]. Fiind vorba
de cel mult 2 parcurgeri ( o data cand este copil, si o data cand esti parinte, in cel mai rau caz), eficienta acestei operatii este O(n).

Reprezentarea 3)
	Pentru aceasta transformare vom folosi vectorul indexChildren care ne va permite sa stim exact cati copii avem si sa-i distribuim pe dreapta si stanga. Mai exact, am folosit o structura de tip arbore bina
(key,left,right) cu mentiunea ca, pe masura ce construim arborele, sub arborele drept va avea intotdeauna o structura de lista inlantuita de noduri. Asadar, pe stanga vom pune intotdeauna cheia primului copil,
iar daca avem mai mult de un copil vom crea legaturi de lista inlantuita intre ceilalti copii, pana atingem indexChildren[i]-1 care reprezinta ultimul copil al nodului i de la punctul anterior. In termeni de memorie,
ne folosim deja de acel vector creat anterior, iar eficienta acestei operatii este O(n) deoarece fiecare nod va fi procesat maxim de 2 ori, fie cand e parinte, fie cand e copil, urmand a fi pus la locul lui in 
timp liniar. 
*/

#include <stdio.h>
#include <tchar.h>
#include <stdlib.h>
#define DIM 9
typedef struct node1 {
	int key;
	struct node1 *parent;
	int afisat = 0;
} NodeT1;

typedef struct multi {
	int key;
	multi *children[DIM+1];
}NodeMulti;

typedef struct binary {
	int key;
	binary *left;
	binary *rightSibling;
}NodeBinary;


NodeT1* createNode(int key) {
	NodeT1 *nod = (NodeT1*)malloc(sizeof(NodeT1));
	nod->parent = NULL;
	nod->key = key;
	nod->afisat = 0;
	return nod;
}
NodeBinary* createNodeBinary(int key) {
	NodeBinary *node = (NodeBinary*)malloc(sizeof(NodeBinary));
	node->key = key;
	node->left = NULL;
	node->rightSibling = NULL;
	return node;
}
int findRoot(int array[], int n) {
	for (int i = 1; i <= n; i++)
		if (array[i] == -1)
			return i;
}


/*void createTreeFirst(int parent[], int n,NodeT1** noduri) {
	for (int i = 1; i <= n; i++)
		noduri[i] = createNode(i);
	NodeT1* root = NULL;
	for(int i=1;i<=n;i++)
		if (parent[i] != -1) {
			noduri[i]->parent = noduri[parent[i]];
		}
		else noduri[i]->parent = NULL;
}
void afisarePrima(NodeT1** noduri) {
	for (int i = 1; i <= 9; i++)
		if (noduri[i]->parent != NULL)
			printf("Nodul %d il are parinte pe %d\n", noduri[i]->key, noduri[i]->parent->key);
		else printf("Nodul %d este radacna\n", noduri[i]->key);

}
void afisare(NodeT1* nod) {
	if (nod->afisat == 0) {
		printf("        %d", nod->key);
		nod->afisat = 1;
		if (nod->parent != NULL)
			afisare(nod->parent);
		if (nod->parent == NULL)
			printf("\n");
	}
}
void prettyPrint1(NodeT1** noduri) {
	for (int i = 1; i <= 9; i++)
	{
		afisare(noduri[i]);
		printf("\n");
	}

}*/
void prettyPrintR1(int key, int level,int parents[],int n) {
	for (int i = 0; i <+ level; i++)
		printf("   ");
	printf("%d\n", key);
	for (int i = 1; i <= n; i++)
		if (parents[i] == key)
			prettyPrintR1(i, level + 1,parents,n);
}
void prettyPrintR2(int key, int level,NodeMulti multi[DIM+1],int indexChildren[DIM+1]) {
	for (int i = 0; i < level; i++)
		printf("   ");
	printf("%d\n", key);
	for (int i = 1; i < indexChildren[key]; i++)//printare fiecarui copil
		prettyPrintR2(multi[key].children[i]->key, level + 1,multi, indexChildren);//mers in adancime cat timp mai avem copii
}
void transform1(NodeMulti multi[], int indexChildren[], int parent[])
{
	indexChildren[0] = 0;
	for (int i = 1; i <= DIM; i++)
	{
		indexChildren[i] = 1;//pozitie copil in vector
		multi[i].key = i;
	}//setez cheie ptr fiecare nod din arbore
	for (int i = 1; i <= DIM; i++)
		if(parent[i]!=-1){
			multi[parent[i]].children[indexChildren[parent[i]]] = &multi[i];//copilul se va pune in vectorul corespunzator parintelui lui (i-copil, parent[i]-parintele copilului "i")
			indexChildren[parent[i]] += 1;//cresc index unde urmeaza a fi pus urmatorul copil
		}
}
NodeBinary* transform2(NodeMulti multi[DIM + 1], int indexChildren[DIM + 1],int root) {
	NodeBinary* binary[DIM + 1];
	for (int i = 1; i <= DIM; i++) {
		binary[i] = createNodeBinary(i);
	}
	for (int i = 1; i <= DIM; i++) {
		if (indexChildren[i] != 1) {
			binary[i]->left = binary[multi[i].children[1]->key];//left va fi intotdeauna primul din vectorul de copii
		}
		for(int j=2;j<indexChildren[i];j++)
			binary[multi[i].children[j-1]->key]->rightSibling = binary[multi[i].children[j]->key];//o lista inlantuita intre fii
	}
	return binary[root];
}
void prettyPrintR3(NodeBinary* tree, int level) {
	if (tree != NULL) {
		if(level!=0)
			for (int i = 0; i < level; i++)
				printf("   ");
		printf("%d\n", tree->key);
		prettyPrintR3(tree->left,level + 1);
		prettyPrintR3(tree->rightSibling,level);
	}
}



int main()
{
	//printf("Constructie 1:\n");
	int parent[DIM+1] = {0,2, 7, 5, 2, 7, 7, -1, 5, 2};
	//NodeT1* noduri[10];
	//createTreeFirst(parent, 9,noduri);
	//afisarePrima(noduri);
	//prettyPrint1(noduri);

	//REPREZENTAREA 1
	int n = (sizeof(parent) / sizeof(int));
	printf("Reprezentarea 1:\n");
	prettyPrintR1(findRoot(parent,n), 0,parent,n);
	//////////////////////////////////////////////////////////



	//REPREZENTAREA 2 (reprezentare 1-standard + transformare)
	NodeMulti multi[DIM + 1]; //arbore multicai
	int indexChildren[DIM + 1];//ultimul index pentru fiecare vector de copii (vom stii exact cati copii avem in multi[i] - adica indexChildren[i]-1)
	transform1(multi, indexChildren, parent);
	printf("Reprezentarea 2:\n");
	prettyPrintR2(findRoot(parent, n), 0, multi, indexChildren);
	///////////////////////////////////////////////////////////


	//REPREZENTAREA 3 
	NodeBinary* Tree = transform2(multi, indexChildren, findRoot(parent, n));
	printf("Reprezentarea 3:\n");
	prettyPrintR3(Tree, 0);

    return 0;
}

