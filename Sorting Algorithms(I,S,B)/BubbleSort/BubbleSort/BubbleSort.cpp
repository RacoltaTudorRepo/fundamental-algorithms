#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>


int main()
{
	FILE *g;
	fopen_s(&g, "bubble.csv", "w");
	for (int n = 100; n <= 10000; n += 500)
	{
	int a[10000], i, aux, f, x, comp = 0, atrib = 0;
	for (i = 0; i < n; i++)
		a[i] = rand() % n;
	x = n;
	do {
		f = 0;
		for (i = 0; i < n - 1; i++)
		{
			comp++;
			if (a[i] > a[i + 1])
			{
				aux = a[i];
				a[i] = a[i + 1];
				a[i + 1] = aux;
				atrib += 3;
				f = 1;
			}
		}
		n--;
	} while (f == 1);
	fprintf(g, "%d,%d,%d,%d\n", n, comp, atrib, comp + atrib);
	}
	return 0;
}



