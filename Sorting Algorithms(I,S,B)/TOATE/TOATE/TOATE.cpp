/* Racolta Tudor Ioan, grupa 30226 - Tema 1: Bubble Sort, Insertion Sort, Selection Sort
Interpretare rezultate:

	1)CAZ FAVORABIL
	Pentru cele 3 metodele, cazul favorabil este sirul sortat.
	Astfel, in termen de comparatii, Selection Sort sta cel mai prost la acel capitol, in timp ce Bubble si Insertion se comporta similar.
	In termen de atribuiri, Selection si Bubble se comporta ideal, efectuand 0 atribuiri, in timp ce atribuirile din Insertion cresc liniar.
	Ca si efort total depus in acest caz,Insertion si Bubble se comporta aproape similar, efectuand sortarea in O(n) in timp ce efortul depus de Selection
este unul substantial, ducand la O(n^2)

	2) CAZ DEVAROBAIL
	Pentru cele 3 metode, cazul favorabil folosit a fost sirul sortat descrescator (pentru Selection s-ar fi putut folosi o permutare circulara)
	In termen de comparatii, toate cele 3 metodele se comporta aproape similar, diferentele fiind mai clare la dimensiuni mari.
	In termen de atribuiri, Selection se comporta cel mai bine, urmat de Insertion iar in final, cel mai costisitor fiind Bubble.
	Datorita diferentei date de atribuiri, efortul total este influentat in special de acestea, comportamentul fiind similar cu cazul atribuirilor.
	Pe baza graficelor, toate cele 3 metode duc la O(n^2) pentru efortul total, Bubble comportandu-se cel mai bine, iar Selection cel mai prost.

	3) CAZ MEDIU-STATISTIC
	S-au generat siruri aleatoare pentru fiecare din 500 in 500 pana la 10000, de 5 ori, si s-a folosit acelasi array pentru fiecare metoda.
	In termen de comparatii, Bubble Sort si Selection Sort se comporta aproximativ similar, efectuand un numar aproape dublu de comparatii decat
in cazul Insertion Sortului.
	In termen de atribuiri, Selection Sort efectueaza mult mai putine atribuiri decat celalalte doua, in timp ce interschimbarea produsa in Bubble Sort
influenteaza vizibil numarul de atribuiri efectuate de aceasta metoda, fiind foarte mare.
	Ca si efort total, Insertion Sort si Selection Sort se comporta similar, cu diferente mici intre numarul de operatii facute, in timp ce Bubble Sort este de departe
cel mai ineficient algoritm de sortare dintre cei 3 datorita numarului extrem de mare de atribuiri.
	Chiar si asa, cu mentiunea ca cele 2 se comporta relativ bine, toate cele 3 metodele raman in O(n^2) si in acest caz mediu statisc.
*/

#include "stdafx.h"
FILE *g;
int compi, atribi, compb, atribb, comps, atribs, compmi, compmb, compms, atribmi, atribmb, atribms;
void InsertionSort(int a[], int n){
	int i, x, j, compi = 0, atribi = 0;
	for (i = 1; i < n; i++)
	{
		x = a[i];
		atribi++;
		j = i - 1;
		compi++;
		while (a[j] > x && j != 1)
		{	
			compi++;
			a[j + 1] = a[j];
			atribi++;
			j--;

		}
		if (j != i - 1)
		{
			a[j + 1] = x;
			atribi++;
		}
	}
	fprintf(g, "%d,%d,%d,%d\n", n, compi, atribi, compi + atribi);
}

void BubbleSort(int a[], int n) {
	int x, i, f, compb = 0, atribb = 0, aux;
	x = n;
	do {
		f = 0;
		for (i = 0; i < n - 1; i++)
		{
			compb++;
			if (a[i] > a[i + 1])
			{
				aux = a[i];
				a[i] = a[i + 1];
				a[i + 1] = aux;
				atribb += 3;
				f = 1;
			}
		}
		n--;
	} while (f == 1);
	fprintf(g, "%d,%d,%d,%d\n", x, compb, atribb, compb + atribb);
}

void SelectionSort(int a[], int n) {
	int i, pos, comps = 0, atribs = 0, aux,j;
	for (i = 0; i<n - 1; i++)
	{
		pos = i;
		for (j = i + 1; j < n; j++)
		{
			if (a[j] < a[pos])
				pos = j;
			comps++;
		}
		if (i != pos)
		{
			atribs += 3;
			aux = a[i];
			a[i] = a[pos];
			a[pos] = aux;
		}
	}
	fprintf(g, "%d,%d,%d,%d\n", n, comps, atribs, comps + atribs);
}


int main()
{
	int n, a[10000];
	fopen_s(&g, "graficeMEDIU.csv", "w");
	for (int j = 1; j <= 5; j++) //se vor genera 5 masuratori 
	{
		fprintf(g, "Masurarea %d,Masurarea %d,Masurarea %d,Masurarea %d,Masurarea %d\n", j, j, j, j, j);
		for (n = 100; n <= 10000; n += 500) //se va variaza dimensiunea sirului folosit
		{
			int i;
			for (i = 0; i <= n; i++)
				a[i] = rand() % n; //se vor genera numere random de la 0 la n, dupa care in .csv se vor copia randurile pe caterogii(ex: Bubble cu n de la 100 pana la final)
			fprintf(g, "Insertion-n,comparatii,atribuiri,comp+atrib\n");
			InsertionSort(a, n); //si s-a folosit functia average in excel pentru calcularea parametrilor medii statistici
			fprintf(g, "Bubble-n,comparatii,atribuiri,comp+atrib\n");
			BubbleSort(a, n);
			fprintf(g, "Selection-n,comparatii,atribuiri,comp+atrib\n");
			SelectionSort(a, n);
		}
	}
    return 0;
}

