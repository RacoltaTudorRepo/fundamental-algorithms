// InsertionSort.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>


int main()
{
	int a[10], n, i, j, x, comp = 0, atrib = 0;
	scanf_s("%d", &n);
	for (i = 0; i < n; i++)
		scanf_s("%d", &a[i]);
	for (i = 1; i < n; i++)
	{
		x = a[i];
		j = i - 1;
		while (a[j] > x && j != 1)
		{
			comp++;
			a[j + 1] = a[j];
			atrib++;
			j--;

		}
		a[j + 1] = x;
		atrib++;
	}
	for (i = 0; i < n; i++)
		printf("%d ", a[i]);
    return 0;
}

