#include "stdafx.h"
#include <stdio.h>
#include <stdlib.h>

int main()
{
	int a[100], i, pos, j, aux, n;
	int atrib = 0, comp = 0;
	scanf_s("%d", &n);
	for (i = 0; i<n; i++)
		scanf_s("%d", &a[i]);
	for (i = 0; i<n - 1; i++)
	{
		pos = i;
		for (j = i + 1; j<n; j++)
			if (a[j]<a[pos]) {
				pos = j;
				comp++;
			}
		if (i != pos)
		{
			atrib += 3;
			aux = a[i];
			a[i] = a[pos];
			a[pos] = aux;
		}
	}
	for (i = 0; i<n; i++)
		printf("%d ", a[i]);
	printf("\nComparatii %d si Atribuiti %d", comp, atrib);

	return 0;
}